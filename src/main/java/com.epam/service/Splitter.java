package com.epam.service;

import com.epam.models.Sentence;
import com.epam.models.Word;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Splitter {

    public List<Sentence> splitText(String lines) {
        lines = trimSpaces(lines);
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        List<Sentence> sentences = new ArrayList<>();
        List<Word> words = new ArrayList<>();
        iterator.setText(lines);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            sentences.add(new Sentence(lines.substring(start, end)));
        }
        return sentences;
    }

    private String trimSpaces(String str) {
        return str.replaceAll("\\s+", " ").trim();
    }
}

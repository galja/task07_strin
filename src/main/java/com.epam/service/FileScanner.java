package com.epam.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileScanner {
    private static Logger logger;
    private String text;

    public FileScanner() {
        logger = LogManager.getLogger(FileScanner.class);
        readFile();
    }

    public String getText(){
        return text;
    }

    private void readFile() {
        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader br = Files.newBufferedReader(Paths.get("book.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
        } catch (IOException e) {
            logger.error("Cannot open file");
        }
        text = stringBuilder.toString();
    }

}

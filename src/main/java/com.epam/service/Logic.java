package com.epam.service;

import com.epam.models.*;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;

public class Logic {
    private static final int FIRST_ELEMENT = 0;
    private static final String QUESTION_MARK = "?";

    private List<Sentence> sentences;
    private List<Word> words;

    public Logic() {
        sentences = new ArrayList<>();
        words = new ArrayList<>();
        initializeFields();
    }

    private void initializeFields() {
        String textFromFile = new FileScanner().getText();
        sentences = new Splitter().splitText(textFromFile);
        for (Sentence sent : sentences) {
            words.addAll(sent.getWords());
        }
    }

    public List<Sentence> sortSentencesByWordsNumber() {
        return sentences.stream()
                .sorted(Comparator.comparingInt(Sentence::length))
                .collect(Collectors.toList());
    }

    public List<Word> sortByFirstConsonant() {
        List<Word> vowelWords = new ArrayList<>();
        words = words.stream().distinct().collect(Collectors.toList());
        for (Word word : words) {
            if (word.getLetters().get(FIRST_ELEMENT).isVowel()) {
                vowelWords.add(word);
            }
        }
        vowelWords.sort(this::compareByFirstConsonant);
        return vowelWords;
    }

    public List<Word> findUniqueWordsInFirstSentence() {
        List<Word> uniqueWodsInFirstSent = new LinkedList<>();
        Set<Word> dictionary = new LinkedHashSet<>(words);
        for (Word wordToCheck : sentences.get(0).getWords()) {
            for (Word uniqueWord : dictionary) {
                if (wordToCheck.equals(uniqueWord)) {
                    uniqueWodsInFirstSent.add(wordToCheck);
                }
            }
        }
        return uniqueWodsInFirstSent;
    }

    public List<Word> sortByLetterFrequencyAsc(char character) {
        return getLetterFrequency(character).entrySet().stream()
                .sorted(this::compareByFrequencyAsc)
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<Word> sortByLetterFrequencyDesc(char character) {
        return getLetterFrequency(character).entrySet().stream()
                .sorted(this::compareByFrequencyDesc)
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<Word> removeConsonantWordsByLength(int length) {
        words = new LinkedList<>(words);
        List<Word> itemsToRemove = words.stream()
                .filter(w -> (w.length() == length) || !(w.getLetters().get(FIRST_ELEMENT).isVowel()))
                .collect(Collectors.toList());
        words.removeAll(itemsToRemove);
        return words;
    }

    public int getMaxSentencesNumberWithSameWords() {
        Map<Word, Integer> eachWordOccurrenceNumber = getEachWordOccurrenceNumber();
        Map.Entry<Word, Integer> maxSentencesNumberWithSameWords = eachWordOccurrenceNumber.entrySet()
                .stream().max(comparingByValue()).get();
        return maxSentencesNumberWithSameWords.getValue();
    }

    public List<Word> findWordsAccordingToLengthInQuestions(int length) {
        List<Sentence> questionSentences = getQuestionSentences();
        List<Word> wordsWithGivenLength = new LinkedList<>();
        for (Sentence sentence : questionSentences) {
            wordsWithGivenLength.addAll(sentence.getWords());
        }
        wordsWithGivenLength = wordsWithGivenLength.stream()
                .distinct()
                .filter(wordInSentence -> wordInSentence.length() == length)
                .collect(Collectors.toList());
        return wordsWithGivenLength;
    }

    private List<Sentence> getQuestionSentences() {
        List<Sentence> questionSentences = new LinkedList<>();
        for (Sentence s : sentences) {
            if (s.getLine().contains(QUESTION_MARK)) {
                questionSentences.add(s);
            }
        }
        return questionSentences;
    }

    private Map<Word, Integer> getEachWordOccurrenceNumber() {
        Set<Word> dictionary = new LinkedHashSet<>(words);
        Map<Word, Integer> eachWordOccurrenceNumber = new HashMap<>();
        for (Word dictionaryWord : dictionary) {
            int wordOccurrenceNumber = 0;
            for (Sentence sent : sentences) {
                if (sent.getWords().stream().anyMatch(w -> dictionaryWord.toString().equals(w.toString()))) {
                    wordOccurrenceNumber++;
                }
            }
            eachWordOccurrenceNumber.put(dictionaryWord, wordOccurrenceNumber);
        }
        return eachWordOccurrenceNumber;
    }

    public Map<Word, Integer> sortWordsInListByFrequency() {
        return getEachWordOccurrenceNumber().entrySet().stream().
                sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).
                collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }

    private int compareByFrequencyAsc(Map.Entry<Word, Integer> o1, Map.Entry<Word, Integer> o2) {
        if (o1.getValue().equals(o2.getValue())) {
            return o1.getKey().compareTo(o2.getKey());
        } else {
            return o1.getValue() - o2.getValue();
        }
    }

    private int compareByFrequencyDesc(Map.Entry<Word, Integer> o1, Map.Entry<Word, Integer> o2) {
        if (o1.getValue().equals(o2.getValue())) {
            return o1.getKey().compareTo(o2.getKey());
        } else {
            return o2.getValue() - o1.getValue();
        }
    }

    private Map<Word, Integer> getLetterFrequency(char charecter) {
        words = words.stream().distinct().collect(Collectors.toList());
        Letter givenLetter = new Letter(charecter);
        Map<Word, Integer> wordsMap = new HashMap<>();
        for (Word word : words) {
            int vowelCount = 0;
            for (Letter letter : word.getLetters()) {
                if (letter.equals(givenLetter)) {
                    vowelCount++;
                }
            }
            wordsMap.put(word, vowelCount);
        }
        return wordsMap;
    }

    private int compareByFirstConsonant(Word firstWord, Word secondWord) {
        Letter firstWordConsonant = getFirstConsonant(firstWord);
        Letter secondWordConsonant = getFirstConsonant(secondWord);
        if (firstWordConsonant == null && secondWordConsonant == null) {
            return 0;
        } else if (firstWordConsonant == null) {
            return -1;
        } else if (secondWordConsonant == null) {
            return 1;
        } else {
            return firstWordConsonant.compareTo(secondWordConsonant);
        }
    }

    private Letter getFirstConsonant(Word word) {
        Letter consonant = null;
        for (Letter letter : word.getLetters()) {
            if (!letter.isVowel()) {
                consonant = letter;
                break;
            }
        }
        return consonant;
    }

    public List<Sentence> swapTwoWords() {
        List<Sentence> changedSentences = new LinkedList<>(sentences);

        for (Sentence sent : changedSentences) {
            Word vowelWord = findWordStartWithVowel(sent);
            Word longestWord = findLongestWord(sent);
            if (((vowelWord != null) && (longestWord != null)) && (!(vowelWord.equals(longestWord))))
                Collections.swap(sent.getWords(), sent.indexOf(vowelWord), sent.indexOf(longestWord));
        }
        return changedSentences;
    }

    public List<Word> sortByFirstLetter() {
        return words.stream()
                .distinct()
                .sorted(Comparator.comparing(e -> e.getLetters().get(FIRST_ELEMENT)))
                .collect(Collectors.toList());
    }

    public List<Word> sortWordsByVowelPercentage() {
        return countVowelsPercentage().entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private Map<Word, Double> countVowelsPercentage() {
//        Map<Word, Long> collect =
//                words.stream().collect(groupingBy(Function.identity(), countVowels()));

        Set<Word> dictionary = new LinkedHashSet<>(words);
        Map<Word, Double> wordsMap = new HashMap<>();
        for (Word word : dictionary) {
            double percentage = (double) countVowels(word) / (double) word.length();
            wordsMap.put(word, percentage);
        }
        return wordsMap;
    }

    private long countVowels(Word word) {
        return word.getLetters().stream().filter(Letter::isVowel).count();
    }

    private Word findWordStartWithVowel(Sentence sentence) {
        Word vowelWord = null;
        for (Word word : sentence.getWords()) {
            if (word.getLetters().get(FIRST_ELEMENT).isVowel()) {
                vowelWord = word;
                break;
            }
        }
        return vowelWord;
    }

    private Word findLongestWord(Sentence sentence) {
        return sentence.getWords()
                .stream().max(Comparator.comparingInt(Word::length))
                .orElse(null);
    }

    private Sentence removeSubstringBetweenTwoSymbols(Sentence sentence, String firstSymbol, String lastSymbol) {
        String sentenceLine = sentence.getLine();
        int beginning = sentenceLine.indexOf(firstSymbol);
        int end = sentenceLine.lastIndexOf(lastSymbol);
        if (sentenceLine.contains(firstSymbol) && sentenceLine.contains(lastSymbol) && beginning <= end) {
            String newSentence = sentenceLine.substring(0, beginning) + sentenceLine.substring(end + 1);
            sentence.setWords(new Sentence(newSentence).getWords());
        }
        return sentence;
    }

    public List<Sentence> removeSubstringBetweenTwoSymbolsInText(String start, String end) {
        return sentences.stream().map(sentence -> removeSubstringBetweenTwoSymbols(sentence, start, end))
                .collect(Collectors.toList());
    }
}

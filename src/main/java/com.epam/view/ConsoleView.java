package com.epam.view;

import com.epam.controller.TextController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class ConsoleView {
    private static final String ENTER_LENGTH = "Enter length, please";
    private static final String ENTER_CHARACTER = "Enter character, please";
    private static final String ENTER_STRING = "Enter character, please";
    private static final String MENU = "Menu";
    private static final int FIRST_ELEMENT = 0;
    private TextController textController;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Locale locale;
    ResourceBundle bundle;

    public ConsoleView() {
        textController = new TextController();
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::showMaxSentencesNumberWithSameWords);
        methodsMenu.put("2", this::showSentencesSortedByWordsNumber);
        methodsMenu.put("3", this::showUniqueWordsInFirstSentence);
        methodsMenu.put("4", this::showWordsAccordingToLengthInQuestions);
        methodsMenu.put("5", this::showSwappedTwoWords);
        methodsMenu.put("6", this::showWordsSortedByFirstLetter);
        methodsMenu.put("7", this::sortWordsByVowelPercentage);
        methodsMenu.put("8", this::showWordsSortedByFirstConsonant);
        methodsMenu.put("9", this::showWordsSortedByLetterFrequencyAsc);
        methodsMenu.put("10", this::showWordsInListSortedByFrequency);
        methodsMenu.put("11", this::showRemovedSubstringBetweenTwoSymbolsInText);
        methodsMenu.put("12", this::showListAfterRemovingConsonantWordsByLength);
        methodsMenu.put("13", this::showWordsSortedByLetterFrequencyDesc);
    }

    private void showWordsSortedByLetterFrequencyDesc() {
        printList(textController.sortByLetterFrequencyDesc(scanString(ENTER_CHARACTER).charAt(FIRST_ELEMENT)));
    }

    private void showListAfterRemovingConsonantWordsByLength() {
        printList(textController.removeConsonantWordsByLength(scanAnInteger(ENTER_LENGTH)));
    }

    private void showRemovedSubstringBetweenTwoSymbolsInText() {
        String firstParameter = scanString(ENTER_STRING);
        String secondParameter = scanString(ENTER_STRING);
        printList(textController.removeSubstringBetweenTwoSymbolsInText(firstParameter, secondParameter));
    }

    private void showWordsInListSortedByFrequency() {
        printMap(textController.sortWordsInListByFrequency());
    }

    private void showWordsSortedByLetterFrequencyAsc() {
        printList((textController.sortByLetterFrequencyAsc(scanString(ENTER_CHARACTER).charAt(FIRST_ELEMENT))));
    }

    private void showWordsSortedByFirstConsonant() {
        printList(textController.sortByFirstConsonant());
    }

    private void sortWordsByVowelPercentage() {
        printList(textController.sortWordsByVowelPercentage());
    }

    private void showWordsSortedByFirstLetter() {
        printList(textController.sortByFirstLetter());
    }

    private void showSwappedTwoWords() {
        printList(textController.swapTwoWords());
    }

    private void showWordsAccordingToLengthInQuestions() {
        printList(textController.findWordsAccordingToLengthInQuestions(scanAnInteger(ENTER_LENGTH)));
    }

    private void showUniqueWordsInFirstSentence() {
        printList(textController.findUniqueWordsInFirstSentence());
    }

    private void showSentencesSortedByWordsNumber() {
        printList(textController.sortSentencesByWordsNumber());
    }

    private void showMaxSentencesNumberWithSameWords() {
        logger.info("Max sentences number with same words: "
                + textController.getMaxSentencesNumberWithSameWords());
    }

    private void printList(List<String> stringList) {
        stringList.forEach(s -> logger.info(s + "\n"));
    }

    private int scanAnInteger(String message) {
        logger.info(message);
        Scanner in = new Scanner(System.in);
        int value = 0;
        try {
            value = in.nextInt();
            if (value <= 0) {
                scanAnInteger("Input must be positive number.\nTry again.\n");
            }
        } catch (InputMismatchException e) {
            logger.error("Wrong input");
        }
        return value;
    }

    private void chooseMenu() {
        logger.info("Chose a language: \n" + "1 - Ukrainian\n" + "2 -  English \n" + "3 - French");
        try {
            int choise = input.nextInt();
            if (choise == 1)
                internationalizeMenuUkrainian();
            if (choise == 2)
                internationalizeMenuEnglish();
            if (choise == 3)
                internationalizeMenuFrench();
        } catch (Exception e) {
            logger.warn("Enter 1, 2 or 3, please");
        }
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("11", bundle.getString("11"));
        menu.put("12", bundle.getString("12"));
        menu.put("13", bundle.getString("13"));
    }


    public void choose() {
        chooseMenu();
        show();
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuFrench() {
        locale = new Locale("fr");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void printMap(Map<String, Integer> map) {
        map.entrySet().forEach(System.out::println);
    }

    private String scanString(String message) {
        logger.info(message);
        Scanner in = new Scanner(System.in);
        String value = null;
        try {
            value = in.next();
        } catch (InputMismatchException e) {
            logger.error("Wrong input");
        }
        return value;
    }

}

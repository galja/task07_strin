package com.epam.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Word implements Comparable {
    private List<Letter> letters;

    Word(String word) {
        letters = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            letters.add(new Letter(word.charAt(i)));
        }
    }

    public String toString() {
        return letters.stream().map(Letter::toString).collect(Collectors.joining(""));
    }

    public List<Letter> getLetters() {
        return letters;
    }

    public int length() {
        return letters.size();
    }

    @Override
    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(letters, word.letters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(letters);
    }
}

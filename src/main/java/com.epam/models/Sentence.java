package com.epam.models;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
    private List<Word> words;
    private String line;

    public Sentence(String line) {
        this.line = line;
        words = new ArrayList<>();
        this.words = splitIntoWordsList(line);
    }

    public List<Word> getWords() {
        return words;
    }

    public String getLine() {
        return line;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public void setLine(String line) {
        this.line = line;
    }
//
//    public void add(Word word) {
//        words.add(word);
//    }

    public int length() {
        return words.size();
    }

    @Override
    public String toString() {
        return words +" ";
    }

    private List<Word> splitIntoWordsList(String text) {
        String[] tempList = text.split("\\W+");
        for (String s : tempList) {
            if (!s.isEmpty()) {
                Word word = new Word(s.toLowerCase());
                words.add(word);
            }
        }
        return words;
    }

    public int indexOf(Word word) {
        int index = 0;
        for (Word w : words) {
            if (w.equals(word)) {
                break;
            }
            index++;
        }
        return index;
    }
}
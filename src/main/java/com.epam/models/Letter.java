package com.epam.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Letter implements Comparable<Letter> {
    private static final Pattern REGEX_VOWEL = Pattern.compile("[aeuioAEUIO]");

    private char symbol;

    public Letter(char symbol) {
        this.symbol = symbol;
    }

    public Boolean isVowel() {
        Matcher matcher = REGEX_VOWEL.matcher(String.valueOf(this.symbol));
        return matcher.matches();
    }

    @Override
    public String toString() {
        return String.valueOf(symbol);
    }

    @Override
    public int compareTo(Letter o) {
        return Character.compare(symbol, o.symbol);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Letter letter1 = (Letter) o;

        return symbol == letter1.symbol;
    }
}

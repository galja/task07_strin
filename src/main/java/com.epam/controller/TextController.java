package com.epam.controller;

import com.epam.models.*;
import com.epam.service.Logic;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TextController {

    private Logic logic;

    public TextController() {
        logic = new Logic();
    }

    //1
    public int getMaxSentencesNumberWithSameWords() {
        return logic.getMaxSentencesNumberWithSameWords();
    }

    //2
    public List<String> sortSentencesByWordsNumber() {
        return sentensestoStringList(logic.sortSentencesByWordsNumber());
    }

    //3
    public List<String> findUniqueWordsInFirstSentence() {
        return wordsToStringList(logic.findUniqueWordsInFirstSentence());
    }

    //4
    public List<String> findWordsAccordingToLengthInQuestions(int length) {
        return wordsToStringList(logic.findWordsAccordingToLengthInQuestions(length));
    }

    //5
    public List<String> swapTwoWords() {
        return sentensestoStringList(logic.swapTwoWords());
    }

    //6
    public List<String> sortByFirstLetter() {
        return wordsToStringList(logic.sortByFirstLetter());
    }

    //7
    public List<String> sortWordsByVowelPercentage() {
        return wordsToStringList(logic.sortWordsByVowelPercentage());
    }

    //8
    public List<String> sortByFirstConsonant() {
        return wordsToStringList(logic.sortByFirstConsonant());
    }

    //9
    public List<String> sortByLetterFrequencyAsc(char letter) {
        return wordsToStringList(logic.sortByLetterFrequencyAsc(letter));
    }

    //10
    public Map<String, Integer> sortWordsInListByFrequency() {
        return wordsMapToString(logic.sortWordsInListByFrequency());
    }

    //11
    public List<String> removeSubstringBetweenTwoSymbolsInText(String firstSymbol, String lastSymbol){
        return sentensestoStringList(logic.removeSubstringBetweenTwoSymbolsInText(firstSymbol, lastSymbol));
    }

    //12
    public List<String> removeConsonantWordsByLength(int length) {
        return wordsToStringList(logic.removeConsonantWordsByLength(length));
    }

    //13
    public List<String> sortByLetterFrequencyDesc(char letter) {
        return wordsToStringList(logic.sortByLetterFrequencyDesc(letter));
    }

    private Map<String, Integer> wordsMapToString(Map<Word, Integer> wordIntegerMap) {
        Map<String, Integer> map = new LinkedHashMap<>();
        for (Map.Entry<Word, Integer> entry : wordIntegerMap.entrySet()) {
            map.put(entry.getKey().toString(), entry.getValue());
        }
        return map;
    }

    private List<String> sentensestoStringList(List<Sentence> list) {
        List<String> sentences = new ArrayList<>();
        for (Sentence sentence : list) {
            sentences.add(sentence.toString());
        }
        return sentences;
    }

    private List<String> wordsToStringList(List<Word> list) {
        List<String> words = new ArrayList<>();
        for (Word word : list) {
            words.add(word.toString());
        }
        return words;
    }
}

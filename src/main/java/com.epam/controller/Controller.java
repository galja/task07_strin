//package com.epam.controller;
//
//import com.epam.service.FileScanner;
//import com.epam.model.SentenceSplitter;
//import com.epam.model.WordsSplitter;
//
//import java.util.*;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;
//
//import static java.util.Map.Entry.comparingByValue;
//
//public class Controller {
//    private SentenceSplitter sentenceSplitter;
//    private WordsSplitter wordsSplitter;
//    private FileScanner fileScanner;
//    private List<String> sentenceList;
//
//    private static final Pattern REGEX_STARI_VOWEL = Pattern.compile("\\b[aeuioAEUIO]\\w*\\b");
//    private static final Pattern REGEX_CONTAINS_VOWEL = Pattern.compile("[aeuioAEUIO]");
//
//    public Controller() {
//        wordsSplitter = new WordsSplitter();
//        sentenceSplitter = new SentenceSplitter();
//        fileScanner = new FileScanner();
//        setSentenseList();
//    }
//
//    public List<String> getSentenseList() {
//        return this.sentenceList;
//    }
//
//    public void setSentenseList() {
//        sentenceList = sentenceSplitter.getSplitText(fileScanner.getText());
//    }
////
////    public int getMaxSentencesNumberWithSameWords() {
////        Map<String, Integer> eachWordOccurrenceNumber = getEachWordOccurrenceNumber();
////        Map.Entry<String, Integer> maxSentencesNumberWithSameWords = eachWordOccurrenceNumber.entrySet()
////                .stream().max(comparingByValue()).get();
////        return maxSentencesNumberWithSameWords.getValue();
////    }
//
////    public void findWordsAccordingToLength(int length) {
////        List<String> questionSentences = getQuestionSentences();
////        List<String> question = wordsSplitter.splitTextIntoWordsList(questionSentences.toString()).stream()
////                .distinct()
////                .filter(wordInSentence -> wordInSentence.length() == length)
////                .collect(Collectors.toList());
////        question.forEach(System.out::println);
////    }
//
//
////    public List<String> swapTwoWords() {
////        List<String> changedSentences = new LinkedList<>();
////        for (String sent : sentenceList) {
////            String vowelWord = findWordStartWithVowel(sent);
////            String longestWord = findLongestWord(sent);
////            String swappedSentence = sent;
////            if (((vowelWord != null) && (longestWord != null)) && (!(vowelWord.equals(longestWord)))) {
////                swappedSentence = swap(sent, vowelWord, longestWord);
////            }
////            changedSentences.add(swappedSentence);
////        }
////        return changedSentences;
////    }
//
//    public List<String> sortByFirstLetter() {
//        List<String> list = wordsSplitter.generateDictionary(fileScanner.getText());
//        list.sort(Comparator.comparing(e -> e.substring(0, 1)));
//        return list;
//    }
//
//    public List<String> sortWordsByVowelPercentage() {
//        return countVowelsPercentage().entrySet().stream()
//                .sorted(Map.Entry.comparingByValue())
//                .map(Map.Entry::getKey)
//                .collect(Collectors.toCollection(LinkedList::new));
//    }
//
//    private Map<String, Double> countVowelsPercentage() {
//        List<String> list = wordsSplitter.generateDictionary(fileScanner.getText());
//        Map<String, Double> wordsMap = new HashMap<>();
//        for (String w : list) {
//            int vowelCount = 0;
//            Matcher vowelMatcher = REGEX_CONTAINS_VOWEL.matcher(w);
//            while (vowelMatcher.find()) {
//                vowelCount++;
//            }
//            double percentage = (double) vowelCount / (double) w.length();
//            wordsMap.put(w, percentage);
//        }
//        return wordsMap;
//    }
//
//    private String swap(String sent, String vowelWord, String longestWord) {
//        Pattern p = Pattern.compile(Pattern.quote(vowelWord) + "|" + Pattern.quote(longestWord));
//        Matcher m = p.matcher(sent);
//        StringBuffer sb = new StringBuffer();
//        while (m.find()) {
//            String replacement = vowelWord;
//            if (m.group(0).equals(vowelWord)) {
//                replacement = longestWord;
//            }
//            m.appendReplacement(sb, Matcher.quoteReplacement(replacement));
//        }
//        m.appendTail(sb);
//        return sb.toString();
//    }
//
//    private String findWordStartWithVowel(String sent) {
//        Matcher matcher = REGEX_STARI_VOWEL.matcher(sent);
//        if (matcher.find()) {
//            return matcher.group(0);
//        } else {
//            return null;
//        }
//    }
//
//
//    private String findLongestWord(String sent) {
//        return wordsSplitter.splitTextIntoWordsList(sent).
//                stream().max(Comparator.comparingInt(String::length))
//                .orElse(null);
//
//    }
//
//    public List<String> getQuestionSentences() {
//
//        List<String> questionSentences = new LinkedList<>();
//        for (String s : sentenceList) {
//            if (s.contains("?")) {
//                questionSentences.add(s);
//            }
//        }
//        return questionSentences;
//    }
//
//    private Map<String, Integer> getEachWordOccurrenceNumber() {
//        Map<String, Integer> eachWordOccurrenceNumber = new HashMap<>();
//        for (String dictionaryWord : wordsSplitter.generateDictionary(fileScanner.getText())) {
//            int wordOccurrenceNumber = 0;
//            for (String sent : sentenceList) {
//                if (wordsSplitter.splitTextIntoWordsList(sent).stream().anyMatch(dictionaryWord::contains)) {
//                    wordOccurrenceNumber++;
//                }
//            }
//            eachWordOccurrenceNumber.put(dictionaryWord, wordOccurrenceNumber);
//        }
//        eachWordOccurrenceNumber.entrySet().stream().forEach(e-> System.out.println(e));
//
//        return eachWordOccurrenceNumber;
//    }
//
//    public Map<String, Integer> sortSentences() {
//        Map<String, Integer> sentencesLengthNumber = new HashMap<>();
//        for (String sent : sentenceList) {
//            sentencesLengthNumber.put(sent, wordsSplitter.wordsNumber(sent));
//        }
//        sentencesLengthNumber = sortMapByValue(sentencesLengthNumber);
//        return sentencesLengthNumber;
//    }
//
//    public String findUniqueWordsInFirstSentence() {
//        List<String> uniqueWodsInFirstSent = new LinkedList<>();
//        List<String> uniqueWords = getUniqueWordsInText();
//        for (String wordToCheck : wordsSplitter.splitTextIntoWordsList(sentenceList.get(0))) {
//            for (String uniqueWord : uniqueWords) {
//                if (wordToCheck.equals(uniqueWord)) {
//                    uniqueWodsInFirstSent.add(wordToCheck);
//                }
//            }
//        }
//        return uniqueWodsInFirstSent.stream().collect(Collectors.joining(","));
//    }
//
//    private List<String> getUniqueWordsInText() {
//        return wordsSplitter.splitTextIntoWordsList(fileScanner.getText()).stream()
//                .map(String::toLowerCase)
//                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
//                .entrySet().stream()
//                .filter(e -> e.getValue() == 1)
//                .map(Map.Entry::getKey)
//                .collect(Collectors.toList());
//    }
//
//    private Map<String, Integer> sortMapByValue(Map<String, Integer> map) {
//        return map.entrySet().stream()
//                .sorted(Map.Entry.comparingByValue())
//                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
//    }
//
//
//    public static void main(String[] args) {
//        new Controller().getQuestionSentences();
//    }
//}
//
